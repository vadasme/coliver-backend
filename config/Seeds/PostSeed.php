<?php
use Migrations\AbstractSeed;

/**
 * Post seed.
 */
class PostSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'hour' => '16:04',
                'comment' => 'laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor',
                'author_id' => 1
            ],[
                'hour' => '15:04',
                'comment' => 'est natus enim nihil est dolore omnis voluptatem numquam\net omnis occaecati quod ullam',
                'author_id' => 3
            ],[
                'hour' => '12:04',
                'comment' => 'quia molestiae reprehenderit quasi aspernatur\naut expedita occaecati aliquam eveniet',
                'author_id' => 2
            ],[
                'hour' => '17:04',
                'comment' => 'non et atque\noccaecati deserunt quas accusantium unde odit nobis qui voluptatem\nquia',
                'author_id' => 1
            ],[
                'hour' => '18:04',
                'comment' => 'harum non quasi et ratione\ntempore iure ex voluptates in ratione\nharum architecto',
                'author_id' => 3
            ],[
                'hour' => '11:04',
                'comment' => 'doloribus at sed quis culpa deserunt consectetur qui praesentium\naccusamus',
                'author_id' => 1
            ],[
                'hour' => '12:04',
                'comment' => 'maiores sed dolores similique labore et inventore et\nquasi temporibus esse sunt',
                'author_id' => 2
            ],[
                'hour' => '10:04',
                'comment' => 'sapiente assumenda molestiae atque\nadipisci laborum distinctio aperiam et ab',
                'author_id' => 3
            ],
        ];

        $table = $this->table('posts');
        $table->insert($data)->save();
    }
}
