<?php
use Migrations\AbstractSeed;
use PhpParser\Node\Name;

/**
 * Authors seed.
 */
class AuthorsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Victor Adasme'
            ],
            [
                'name' => 'Ignacio Gonzalez'
            ],
            [
                'name' => 'Diego Adonis'
            ]
        ];

        $table = $this->table('authors');
        $table->insert($data)->save();
    }
}
