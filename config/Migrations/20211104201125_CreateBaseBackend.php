<?php
use Migrations\AbstractMigration;

class CreateBaseBackend extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('posts')
            ->addColumn('hour', 'string', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('comment', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('post_type', 'string', [
                'default' => 'post',
                'null' => true,
            ])
        ->create();

        $this->table('authors')
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
        ->create();
        
        $this->table('posts')
            ->addColumn('author_id','integer',['signed'=>'disable', 'null'=>true])
            ->addForeignKey('author_id','authors','id',['delete'=>'CASCADE','update'=>'NO_ACTION'])
        ->update();
    }
}
