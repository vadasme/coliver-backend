# CakePHP Application Skeleton

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![Total Downloads](https://img.shields.io/packagist/dt/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

A skeleton for creating applications with [CakePHP](https://cakephp.org) 3.x.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Clone this repository
3. Run `composer install` into directory.

### Dentro del proyecto:

Acceda al archivo config/app.php (linea 266) para configurar credenciales de bd.
Una vez que se configuren las credenciales realice los siguiente en el terminal (consola). 

```
    bin\cake migrations migrate
```

Con lo anterior se crean las tablas en la base de datos.
Despues de que se termine de ejecutar lo anterior digite

```
    bin\cake migrations seed --seed DataSeed
```

Inicia la aplicacion con el sgte comando

```
    bin/cake server -p 8765
```